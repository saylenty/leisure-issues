leisure-issues
==============
This repository contains solution for interesting problems I've ever met.<br>
Many of them, as a rule, can be found at the Programming Olympics.<br>
You could notice that many of the solutions here is written in Python3.x.
