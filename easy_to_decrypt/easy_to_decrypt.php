<?php
    /*
    Когда Вито Маретти пишет важное письмо, он его шифрует. Не очень надежным методом, но достаточным для того, чтобы ни
    один детектив не понял ни слова. Шефу полиции такое положение дел очень не нравится. Он хочет взломать шифр Вито и
    обещает забыть о всех Ваших грехах, если Вы сделаете это для него. Детективы расскажут Вам, как устроен шифр.
    В шифре каждое слово шифруется отдельно. Рассмотрим шифрование на примере слов, состоящих только из строчных
    латинских букв. В начале каждая буква заменяется соответствующим ей числом: a на 0, b на 1, c на 2, ..., z на 25.
    После этого к первому числу добавляется 5, ко второму числу добавляется первое число, к третьему — второе и т.д.
    После этого если какое-то число превосходит 25, то оно заменяется остатком от деления этого числа на 26. И, наконец,
    числа обратно заменяются буквами.
    Зашифруем слово secret:
    Шаг 0.   s   e   c   r   e   t
    Шаг 1.   18  4   2   17  4   19
    Шаг 2.   23  27  29  46  50  69
    Шаг 3.   23  1   3   20  24  17
    Шаг 4.   x   b   d   u   y   r
    В итоге получилось слово xbduyr.
    */
    echo "Type your encrypted pass phase: ";
    $handle = fopen ("php://stdin","r");
    $line = str_split(trim(fgets($handle)));
    from_char($line);  # translate pass-phase to convenient variant
    $buf = $line[0] - 5;  # if it is a 'real' item or not
    $prev = $line[0];  # previous element
    $line[0] < 5 ? $line[0] = 26 + $buf : $line[0] = $buf;
    for ($i = 1, $count = count($line); $i < $count; $i++){
        $buf = $line[$i] - $prev;
        $prev = $line[$i];  # save it for further calc-ons
        $buf < 0 ? $line[$i] = $buf + 26 : $line[$i] = $buf;
    }
    to_char($line);  # translate it back
    foreach($line as $item) echo $item; # print decrypted pass-phrase

    function from_char(&$array){
        for($i=0, $count = count($array); $i < $count; $i++){
            $array[$i] = ord($array[$i]) - 97;
        }
    }

    function to_char(&$array){
        for($i=0, $count = count($array); $i < $count; $i++){
            $array[$i] = chr($array[$i] + 97);
        }
    }