#!/usr/bin/python3
# -*- coding: utf-8 -*-
__author__ = 'Saylenty'

""" == Task details ==
Исходные данные
    Целое число N (1 ≤ N ≤ 100) – размер таблички.
Результат
    Табличка, помеченная циферками. Номер один должен находиться в правом верхнем углу,
    далее ячейки нумеруются по диагоналям сверху вниз, последний номер (N*N) стоит в левом нижнем углу.
Пример:
    Исходные данные: N = 3
    Результат
        4 2 1
        7 5 3
        9 8 6
    == End task details ==
"""


def main():
    try:
        m_size = int(input("enter matrix size: "), base=10)
    except ValueError as ex:
        print('Wrong input "1 <= size <= 100" : {}'.format(ex))
        return
    else:
        if m_size <= 0:
            print('Wrong input "1 <= size <= 100"')
            return False
    matrix = [{} for i in range(m_size)]
    matrix[0][0] = 1
    buf = m_size
    for i in range(1, m_size):  # init first column
        matrix[i][0] = matrix[i - 1][0] + i + 1
    for i in range(m_size):
        j = 1
        while j < buf:
            matrix[i][j] = matrix[i][j - 1] + j + i
            j += 1
        k = j - 1
        while j < m_size:
            matrix[i][j] = matrix[i][j - 1] + k + i
            j += 1
            k -= 1
        buf -= 1

    for i in matrix:
        for a, b in enumerate(reversed(tuple(i.values()))):
            if a == m_size - 1:
                print(b, end='')
            else:
                print(b, end='\t')
        print()

if __name__ == "__main__": main()