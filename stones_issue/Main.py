#!/usr/bin/python3
# -*- coding: utf-8 -*-
__author__ = 'Saylenty'

import sys


def divide(sequence, local_sum, middle, answer, best_res):
    """function divides the list into two ones with equal sums -> return only one"""
    i = 0
    while i < len(sequence):  # iterate over sorted list
        local_sum += sequence[i]  # calculate local sum
        if local_sum == int(middle) or local_sum == round(middle):
            answer.append(sequence[i])
            return True  # Success
        answer.append(sequence[i])
        if local_sum > int(middle) or local_sum > round(middle):
            if abs(local_sum - int(middle)) < min(best_res.keys()) if best_res.keys() else sys.maxsize:
                best_res.clear()
                best_res[abs(local_sum - int(middle))] = answer.copy()  # if the difference is lower than we have
            answer.pop()
            return False
        if divide(sequence[i + 1:], local_sum, middle, answer, best_res):
            return True
        else:
            answer.pop()
            local_sum = sum(answer)  # recalculate local sum
        i += 1


def main():  # The main point to the app
    sequence = list(map(int, input().split()))  # read sequence of integers which should be halved
    # answer - perfectly halved list if we have one; best-res - dict which contains the best solution
    answer, best_res, local_sum = [], {}, 0
    middle = sum(sequence) / 2  # find the middle of the list -> float if even
    divide(sorted(sequence), local_sum, middle, answer, best_res)  # call recursive algorithm
    if not answer: answer = best_res[min(best_res.keys())]  # chose the best solution
    for item in answer:  # create the final answer
        sequence.remove(item)
    print(sequence, answer)  # print the result


if __name__ == '__main__': main()